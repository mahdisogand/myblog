const sql = require('../Mssql/Mssql')
const crypt = require('../Encryption/index')


const mainCON = {
    getbanners:(req,res)=>{
        try {
          
            sql.select('select name,proimage,[release-date] AS redate , [show-image] AS shimage ,subject from dft.banner inner join dft.post on dft.banner.Postid = dft.post.Postid inner join dft.users on dft.users.UserID = dft.post.UserID',
            (err,result)=>{
           
           if(result != undefined){
            res.send(result.recordset)
           }
           else{
            res.send('[]')
           }
                
          
                
            })
        } catch (error) {
            res.send(error)
        }
      
      
    },
     getposts:(req,res)=>{
        let {page,hash}= req.body
        if(hash != undefined){
            sql.select(`select UserID from dft.users where email = '${crypt.decryptf(hash)}'`,(err,result)=>{
                  if(err) throw err
                  let id = result.recordset[0].UserID.toString()
        try {
            sql.select(`SELECT PostID ,subtopic, name,text,proimage,[release-date] AS redate , [show-image] AS shimage ,subject ,[like-count] AS Lcount
            FROM dft.post 
             inner join dft.users on dft.post.UserID = dft.users.UserID
            ORDER BY PostID 
            OFFSET ${page} ROWS
            FETCH NEXT 10 ROWS ONLY`,(err,result)=>{
                if(result != undefined){
                    sql.select(`SELECT COUNT(*) AS cpost FROM dft.post`,(err,result2)=>{
                        if(result2 != undefined){
                            if(id != 'n'){
                                sql.select(`SELECT *
                                FROM [daftadb].[dft].[savePosts]
                                where UserID = '${id}'`,(err,result3)=>{
                                    if(err) throw err
                                    res.send({res1:result.recordset,res2:result2.recordset,res3:result3.recordset})
                                })
                            }
                            else{
                                res.send({res1:result.recordset,res2:result2.recordset})
                            }
                          
                          
                        }
                        else{
                             res.send('[]')
                        }
                      
                    })
                }
                else{
                    res.send('[]')
                }
                
                
            })
        } catch (error) {
            res.send(error)
        }
      
    })
}
    },
    gettoppost:(req,res)=>{
        try {
            sql.select(`SELECT TOP(4) subject,[show-image] AS shimage,[release-date] AS redate ,  [like-count] AS Lcount
            FROM [daftadb].[dft].[post]
            ORDER BY  [like-count] DESC, [release-date] DESC 
            `,(err,result)=>{
                if(result != undefined){
                    res.send(result.recordset)
                }
                else{
                    res.send('[]')
                }
                
            })
        } catch (error) {
            res.send(error)
        }
    },
    getselpost:(req,res)=>{
        try {
            sql.select(`SELECT name,text,proimage,[release-date] AS redate , [show-image] AS shimage ,subject ,[like-count] AS Lcount
            FROM dft.post 
			inner join dft.spost on dft.post.PostID = dft.spost.PostID
             inner join dft.users on dft.post.UserID = dft.users.UserID
            `,(err,result)=>{
                if(result != undefined){
                    res.send(result.recordset)
                }
                else{
                    res.send('[]')
                }
            })
        } catch (error) {
            res.send(error)
        }
    }
}


module.exports= mainCON