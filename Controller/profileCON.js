const sql = require('../mssql/mssql.js')
const crypt = require('../Encryption/')

const profileCON = {
    getprofile:(req,res)=>{ 
        let {email} = req.body
        sql.select(`select name ,  family , bio , proimage
        from dft.users 
        where email = '${crypt.decryptf(email)}'`,(err,data)=>{
            sql.select(`select count(email) as count from dft.users
            inner join dft.follows on dft.users.UserID = dft.follows.follow_id 
            where email = '${crypt.decryptf(email)}'`,(err,data1)=>{
                sql.select(`select count(email) as count from dft.users
            inner join dft.follows on dft.users.UserID = dft.follows.follow_id2 
            where email = '${crypt.decryptf(email)}'`,(err,data2)=>{
                res.send({status:true,image:data.recordset[0].proimage,name:data.recordset[0].name+' '+data.recordset[0].family,bio:data.recordset[0].bio,f1:data1.recordset[0].count,f2:data2.recordset[0].count})
            })
            })
        
                })
       
    },
    getfollowers:(req,res)=>{
        let {hash} = req.body
        
        sql.select(`select  email , dft.follows.follow_id2 from dft.users
        inner join dft.follows on dft.users.UserID = dft.follows.follow_id 
        where email = '${crypt.decryptf(hash)}'`,(err,data)=>{
            let q = `select UserID , name , proimage from dft.users where UserID = -1 `
            for (let i = 0; i < data.recordset.length; i++) {
              q += ' or UserID ='+data.recordset[i].follow_id2
            }
            sql.select(`select UserID from dft.users where email = '${crypt.decryptf(hash)}'`,(err,result)=>{
                if(err) throw err
                sql.select(q,(err,data2)=>{
                    sql.select(`select UserID from (select * from dft.follows where follow_id2 = ${result.recordset[0].UserID}) a
                    inner join (select * from dft.follows where follow_id = ${result.recordset[0].UserID}) b on a.follow_id = b.follow_id2
                    inner join dft.users on dft.users.UserID = a.follow_id`,(err,data3)=>{
                        res.send({status:true,d:data2,d2:data3})
                    })
                    
                })
            })
            })
           
                
    },
    getfollowings:(req,res)=>{
        let {hash} = req.body
        sql.select(`select  email , dft.follows.follow_id from dft.users
        inner join dft.follows on dft.users.UserID = dft.follows.follow_id2 
        where email = '${crypt.decryptf(hash)}'`,(err,data)=>{
          
            let q = `select  UserID , name , proimage from dft.users where UserID = -1 `
            for (let i = 0; i < data.recordset.length; i++) {
            
              q += ' or UserID ='+data.recordset[i].follow_id
            }
            sql.select(q,(err,data2)=>{
                for (let i = 0; i < data2.recordset.length; i++) {
                    data2.recordset[i].fstatus = true
                    
                }
                res.send({status:true,d:data2})
            })
                })
    },
    remfollow:(req,res)=>{
        let {f1,hash} = req.body
        sql.select(`select UserID from dft.users where email = '${crypt.decryptf(hash)}'`,(err,result)=>{
            if(err) throw err
            sql.delete(`delete From dft.follows
        where follow_id = ${f1} and follow_id2 = ${result.recordset[0].UserID}`,(err,result)=>{
             res.send({d:result})
        })
        })
      
       
    },
    addfollow:(req,res)=>{
        let {f1,hash} = req.body
        sql.select(`select UserID from dft.users where email = '${crypt.decryptf(hash)}'`,(err,result)=>{
            if(err) throw err
            sql.insert(`insert into dft.follows
        values (${f1},${result.recordset[0].UserID})`,(err,result)=>{
             res.send({d:err})
        })
        })
        
        
        
    },
    getmeposts:(req,res)=>{
        let {hash}= req.body
        sql.select(`select UserID from dft.users where email = '${crypt.decryptf(hash)}'`,(err,result)=>{
            if(err) throw err
            sql.select(`select PostID , subject,subtopic , text , [release-date] as redate , [show-image] as shimage from dft.post
        where  UserID = '${result.recordset[0].UserID}'`,(err,data1)=>{
           res.send({status:true,data:data1})
        })
      })
       

    },
    getsaveposts:(req,res)=>{
        let {hash}= req.body
        sql.select(`select UserID from dft.users where email = '${crypt.decryptf(hash)}'`,(err,result)=>{
            if(err) throw err
            sql.select(` select dft.savePosts.PostID ,subtopic,subject,text,[release-date] as redate , [show-image] as shimage , [like-count] as likec from dft.savePosts
            inner join dft.post on dft.post.PostID = dft.savePosts.PostID
            where dft.savePosts.UserID = '${result.recordset[0].UserID}'`,(err,data1)=>{
               res.send({status:true,data:data1})
            })
      })
        

    },

    remmepost:(req,res)=>{
        let {id,pid}= req.body
        sql.select(`select UserID from dft.users where email = '${crypt.decryptf(hash)}'`,(err,result)=>{
            if(err) throw err
            sql.delete(`delete dft.post
            where UserID = '${result.recordset[0].UserID}' and PostID='${pid}'`,(err,data1)=>{
               res.send({status:true,data:data1})
            })
      })
       
    },

    remsave:(req,res)=>{
        let {hash,pid}= req.body
        sql.select(`select UserID from dft.users where email = '${crypt.decryptf(hash)}'`,(err,result)=>{
            if(err) throw err
            sql.delete(`delete dft.savePosts 
            where UserID = '${result.recordset[0].UserID}' and PostID='${pid}'`,(err,data1)=>{
               res.send({status:true,data:data1})
            })
      })
       

    }
    ,
    addsave: async(req,res)=>{
        let {hash,pid}= req.body
            sql.select(`select UserID from dft.users where email = '${crypt.decryptf(hash)}'`,(err,result)=>{
                  if(err) throw err
                  sql.insert(`insert into dft.savePosts
                  values(${result.recordset[0].UserID},${pid})
                  `,(err,data)=>{
                     res.send({status:true,d:data})
                  })
                  
            })
           
        
       
    }
}

module.exports= profileCON