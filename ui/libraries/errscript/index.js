const errbox = document.getElementById('errbox')
const errboxp = document.getElementById('errbox-p')

function showerr(err){
    errboxp.innerText = err
    errbox.style.display = 'block'
    setTimeout(() => {
        errbox.style.opacity = '1'
    }, 50);
    setTimeout(() => {
        errbox.style.opacity = '0.5'
        errbox.style.display = 'none'
       
    }, 3000);
   
}