let dropbtn = document.getElementById('drop-btn')
let menudrop =document.getElementById('menu-drop')
let menuul = document.getElementById('menu-ul')
dropbtn.addEventListener('click',()=>{
    menudrop.classList.toggle('open-menu')

        menuul.classList.toggle('open-menu')
    dropbtn.classList.toggle('rote-deop-btn')
})


function cvtime(gy, gm, gd) {
  var g_d_m, jy, jm, jd, gy2, days;
  g_d_m = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
  gy2 = (gm > 2) ? (gy + 1) : gy;
  days = 355666 + (365 * gy) + ~~((gy2 + 3) / 4) - ~~((gy2 + 99) / 100) + ~~((gy2 + 399) / 400) + gd + g_d_m[gm - 1];
  jy = -1595 + (33 * ~~(days / 12053));
  days %= 12053;
  jy += 4 * ~~(days / 1461);
  days %= 1461;
  if (days > 365) {
      jy += ~~((days - 1) / 365);
      days = (days - 1) % 365;
  }
  if (days < 186) {
      jm = 1 + ~~(days / 31);
      jd = 1 + (days % 31);
  } else {
      jm = 7 + ~~((days - 186) / 30);
      jd = 1 + ((days - 186) % 30);
  }
  return [jy + '/' + jm + '/' + jd];
}

let fel = new Vue({
  el:'#followers-ul',
  data:{
      users:[]
  }
})


function popup(e) {
  if (e == 'followers') {
    let cookie = document.cookie.split(';')
    for (let i = 0; i <= cookie.length; i++) {
      try {
        if(cookie[i].indexOf('a_usr') != -1){

            $.post('/getfollowers',{hash:cookie[i].split('a_usr=')[cookie[i].split('a_usr=').length-1]},(data)=>{
              for (let i = 0; i < data.d.recordset.length; i++) {
                for (let j = 0; j < data.d2.recordset.length; j++) {
                
                  if(data.d2.recordset[j].UserID == data.d.recordset[i].UserID){
                    data.d.recordset[i].fstatus = true
                    break;
                  }
                  else{
                    data.d.recordset[i].fstatus = false
                  };
                }
                 
               
            }
              fel.users = data.d.recordset
            document.querySelector('#popup-header h3').innerHTML='دنبال کننده ها '
            document.getElementById('overlay').style.display='block'
          document.getElementById('follow').style.display='block'
            })
          
    
        }
      } catch (error) {
        
      }
    
    }
   
  }
  else{
    let cookie = document.cookie.split(';')
    for (let i = 0; i <= cookie.length; i++) {
      try {
        if(cookie[i].indexOf('a_usr') != -1){
          $.post('/getfollowings',{hash:cookie[i].split('a_usr=')[cookie[i].split('a_usr=').length-1]},(data)=>{
          fel.users = data.d.recordset
          document.querySelector('#popup-header h3').innerHTML='دنبال شونده'
          document.getElementById('overlay').style.display='block'
        document.getElementById('follow').style.display='block'
          })
        }
      } catch (error) {
        
      }
  
    }
   
  }
  
}

function hideoverlay(){
  document.getElementById('overlay').style.display='none'
  document.getElementById('follow').style.display='none'
}

function getprofile() {
let cookie = document.cookie.split(';')
for (let i = 0; i <= cookie.length; i++) {
  try {
    if(cookie[i].indexOf('a_usr') != -1){
      $.post('/getprofile',{email:cookie[i].split('a_usr=')[cookie[i].split('a_usr=').length-1]},(data)=>{
       document.getElementById('pm-658').src = data.image
       document.getElementById('nm-698').innerHTML = data.name
       document.getElementById('f1-80').innerHTML = data.f1
       document.getElementById('f1-50').innerHTML = data.f2
       document.getElementById('bio-10').innerHTML = data.bio
       })
     }
  } catch (error) {
    
  }

}
    
}

function follow(p,s) {
  let cookie = document.cookie.split(';')
  if(s == 'un'){
    for (let i = 0; i <= cookie.length; i++) {
      try {
        if(cookie[i].indexOf('a_usr') != -1){
           $.post('/remfollow',{f1:p,hash:cookie[i].split('a_usr=')[cookie[i].split('a_usr=').length-1]},(data)=>{
           if(document.querySelector('#popup-header h3').innerText == 'دنبال کننده ها'){
            popup('followers')
           }
            else{
              popup('following')
            }
            getprofile()
           })
         }
      } catch (error) {
        
      }
    }
  }
  else if (s == 'f') {
    for (let i = 0; i <= cookie.length; i++) {
      try {
        if(cookie[i].indexOf('a_usr') != -1){
           $.post('/addfollow',{f1:p,hash:cookie[i].split('a_usr=')[cookie[i].split('a_usr=').length-1]},(data)=>{
            if(document.querySelector('#popup-header h3').innerText == 'دنبال کننده ها'){
              popup('followers')
             }
              else{
                popup('following')
              }
            getprofile()
           })
         }
      } catch (error) {
        
      }
    }
  }

}

let mepost = new Vue({
  el:'#post-ul',
  data:{
    posts:[]
  }
})

function getposts() {
  let cookie = document.cookie.split(';')
  for (let i = 0; i <= cookie.length; i++) {
    try {
      if(cookie[i].indexOf('a_usr') != -1){
          $.post('/getmeposts',{hash:cookie[i].split('a_usr=')[cookie[i].split('a_usr=').length-1]},(data1)=>{
            for (let i = 0; i < data1.data.recordset.length; i++) {
              data1.data.recordset[i].redate = cvtime(parseInt(data1.data.recordset[i].redate.slice(0,4)),
              parseInt(data1.data.recordset[i].redate.slice(5,7)),
              parseInt(data1.data.recordset[i].redate.slice(8,10)))[0]
          }
     mepost.posts = data1.data.recordset
         })
       }
    } catch (error) {
    }
  }
}

let savepost = new Vue({
  el:'#post-save-ul',
  data:{
    sposts:[]
  }
})

function getsaveposts() {
  let cookie = document.cookie.split(';')
  for (let i = 0; i <= cookie.length; i++) {
    try {
      if(cookie[i].indexOf('a_usr') != -1){
          $.post('/getsaveposts',{hash:cookie[i].split('a_usr=')[cookie[i].split('a_usr=').length-1]},(data1)=>{
            for (let i = 0; i < data1.data.recordset.length; i++) {
              data1.data.recordset[i].redate = cvtime(parseInt(data1.data.recordset[i].redate.slice(0,4)),
              parseInt(data1.data.recordset[i].redate.slice(5,7)),
              parseInt(data1.data.recordset[i].redate.slice(8,10)))[0]
          }
          for (let j = 0; j < data1.data.recordset.length; j++) {
            data1.data.recordset[j].sstatus = true
          }
          savepost.sposts = data1.data.recordset
          mepost.posts = []
         })
        
       }
    } catch (error) {
    }
  }
}

function remmepost(e) {
  let cookie = document.cookie.split(';')
  for (let i = 0; i <= cookie.length; i++) {
    try {
      if(cookie[i].indexOf('a_usr') != -1){
          $.post('/remmepost',{hash:cookie[i].split('a_usr=')[cookie[i].split('a_usr=').length-1],pid:e,},(data1)=>{
            getposts()
         })
       }
    } catch (error) {
    }
  }
}

function removesave(e) {
  let cookie = document.cookie.split(';')
  for (let i = 0; i <= cookie.length; i++) {
    try {
      if(cookie[i].indexOf('a_usr') != -1){
          $.post('/remsave',{hash:cookie[i].split('a_usr=')[cookie[i].split('a_usr=').length-1],pid:e,},(data1)=>{
            getsaveposts()
         })
       }
    } catch (error) {
    }
  }
}

function loadbody() {
  getprofile()
  getposts()
}