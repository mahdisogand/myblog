let dropbtn = document.getElementById('drop-btn')
let menudrop =document.getElementById('menu-drop')
let menuul = document.getElementById('menu-ul')
dropbtn.addEventListener('click',()=>{
    menudrop.classList.toggle('open-menu')

        menuul.classList.toggle('open-menu')
    dropbtn.classList.toggle('rote-deop-btn')
})

function showinp(e) {
let ele =e.parentElement.parentElement.children[3].children[0]
ele.classList.toggle('inp-show')
}


function cvtime(gy, gm, gd) {
    var g_d_m, jy, jm, jd, gy2, days;
    g_d_m = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
    gy2 = (gm > 2) ? (gy + 1) : gy;
    days = 355666 + (365 * gy) + ~~((gy2 + 3) / 4) - ~~((gy2 + 99) / 100) + ~~((gy2 + 399) / 400) + gd + g_d_m[gm - 1];
    jy = -1595 + (33 * ~~(days / 12053));
    days %= 12053;
    jy += 4 * ~~(days / 1461);
    days %= 1461;
    if (days > 365) {
        jy += ~~((days - 1) / 365);
        days = (days - 1) % 365;
    }
    if (days < 186) {
        jm = 1 + ~~(days / 31);
        jd = 1 + (days % 31);
    } else {
        jm = 7 + ~~((days - 186) / 30);
        jd = 1 + ((days - 186) % 30);
    }
    return [jy + '/' + jm + '/' + jd];
}


function getpostdetail() {
    $.post('/getpostdetail',{subject:decodeURI(location.search),id:location.hash},(data)=>{
        if(data.length == 1){
            for (let i = 0; i < data.length; i++) {
                document.querySelector('.back-img').style.backgroundImage='url('+data[i].shimage+')'
                document.querySelector('#post-detail h2').innerHTML=data[i].subject
                document.querySelector('#author img').src=data[i].proimage
                document.querySelector('#author h4').innerHTML=data[i].name
             }
             for (let i = 0; i < data.length; i++) {
                data[i].redate = cvtime(parseInt(data[i].redate.slice(0,4)),
                parseInt(data[i].redate.slice(5,7)),
                parseInt(data[i].redate.slice(8,10)))[0]
                document.querySelector('#release-time span').innerHTML=data[i].redate
                document.querySelector('#post-content').innerHTML = data[i].text
            }
           
        }
   
    })
}

let relatedpost = new Vue({
    el:'#related-ul',
    data:{
        relpost:[]
    }
})

function  getrelatedpost(){
    $.post('/getrelatedpost',{id:location.hash},(data)=>{
        for (let i = 0; i < data.length; i++) {
            data[i].redate = cvtime(parseInt(data[i].redate.slice(0,4)),
            parseInt(data[i].redate.slice(5,7)),
            parseInt(data[i].redate.slice(8,10)))[0]
        }
        console.log(data)
        relatedpost.relpost = data
    })
}


function loadbody(){
    getpostdetail()
    getrelatedpost()
   
}

