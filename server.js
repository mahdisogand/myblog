// server codes

const express  = require('express')
const bodyParser = require('body-parser')
const app = express()
const appRout = require('./Routes')

app.use(express.static(__dirname+'/ui'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))
app.use(appRout)


app.listen('80',()=>{
console.log('server is run :)')
})
  