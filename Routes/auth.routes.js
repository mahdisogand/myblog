const router = require("express").Router()
const authuserCON =require('../Controller/authuserCON')

router.use('/authuser',authuserCON.getuser)
router.use('/checkuser',authuserCON.checkuser)
router.use('/checkcookie',authuserCON.checkcookie)


module.exports = router