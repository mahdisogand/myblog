const router  = require('express').Router()
const mainRoutes = require('./main.routes')
const loginRoutes = require('./login.routes')
const signupRoutes = require('./signup.routes')
const mainloader = require('./mainloader.routes')
const PostRoutes = require('./post.routes')
const postloader = require('./postloader.routes')
const registerloader = require('./registerloader.router')
const emailRoutes = require('./emailRoutes')
const encryption = require('./encryption.routes')
const authrouter = require('./auth.routes')
const profilemeRoutes = require('./profilemeRoutes')
const profileroutes = require('./profile.routes')
const newpostRoutes = require('./newpostRoutes')
const newpostrouter  =require('./newpost.router')

router.use(mainRoutes) 
router.use('/Login',loginRoutes) 
router.use('/Register',signupRoutes)
router.use('/verifyemail',emailRoutes)
router.use('/profile-me',profilemeRoutes)
router.use('/newpost',newpostRoutes)
router.use(PostRoutes)
router.use(mainloader)
router.use(postloader)
router.use(registerloader)
router.use(authrouter)
router.use(profileroutes)
router.use(newpostrouter)


module.exports = router