const router = require('express').Router()
const newpostCON = require('../Controller/newpostCON')
const multer  = require('multer')
const upload = multer({ dest:'E:/imgtest/static/image/' })

router.use('/uploadimg',upload.single('image'),newpostCON.uploadimg)
router.use('/addnewpost',newpostCON.addnewpost)

module.exports=router