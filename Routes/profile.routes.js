
const router = require('express').Router()
const proloader = require('../Controller/profileCON')

router.use('/getprofile',proloader.getprofile)
router.use('/getfollowers',proloader.getfollowers)
router.use('/getfollowings',proloader.getfollowings)
router.use('/remfollow',proloader.remfollow)
router.use('/addfollow',proloader.addfollow)
router.use('/getmeposts',proloader.getmeposts)
router.use('/getsaveposts',proloader.getsaveposts)
router.use('/remsave',proloader.remsave)
router.use('/remmepost',proloader.remmepost)
router.use('/addsave',proloader.addsave)
module.exports = router