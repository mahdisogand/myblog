const router = require('express').Router()
const mainCON = require('../Controller/mainCON')

router.post('/getbanners',mainCON.getbanners)
router.post('/getposts',mainCON.getposts)
router.post('/gettoppost',mainCON.gettoppost)
router.post('/getselpost',mainCON.getselpost)

module.exports=router